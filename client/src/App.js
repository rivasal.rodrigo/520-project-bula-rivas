import './App.css';
import React from 'react';
import StarbucksMapApp from "./components/StarbucksMapApp"
import img from "./utils/coffee.png";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={img} alt="logo" id="headerLogo"></img>
        <p>The World of Starbucks</p>
      </header>
      <div>
        <StarbucksMapApp />
      </div>
      </div>
  );
}

export default App;
