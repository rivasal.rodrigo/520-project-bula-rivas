const configs = {
    attribution: 
    "&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors",
    tileUrl: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    center: [45.49, -73.63],
    bounds: [[-89.5605, 41.9022], [-72.0703, 53.4357]],
    zoom: {minZoom: 8,
      maxZoom: 18,
      initialZoom: 10},
  }
  
export default {configs}
  
