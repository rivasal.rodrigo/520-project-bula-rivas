import React, { Component } from "react";


export default class StarbucksToolTip extends Component {

      render(){
          return(
              <div>
                <h3>Starbucks of {this.props.store["Store Name"]}</h3>
                <p>City: {this.props.store.City}</p>
                <p>Address: {this.props.store["Street Address"]}</p>
                <p>Phone Number: {this.props.store["Phone Number"]}</p>
                <p>Country: {this.props.store.Country}</p>
              </div>
          );
      }
}