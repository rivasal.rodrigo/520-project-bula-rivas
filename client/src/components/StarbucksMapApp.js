import React, { Component } from "react";
import "leaflet/dist/leaflet.css";
import "react-leaflet-markercluster/dist/styles.min.css";
import StarbucksMap from "./StarbucksMap"
import configs from "../utils/config"

export default class StarbucksMapApp extends Component {
  
  constructor(props) {
    super(props);
    this.state = { 
      bounds: configs.configs.bounds,
    };
    this.changeBounds = this.changeBounds.bind(this);
  }

  changeBounds(Bounds){
    this.setState({bounds: [[Bounds.getNorthEast().lng, Bounds.getNorthEast().lat], [Bounds.getSouthWest().lng, Bounds.getSouthWest().lat]]})
  }

  render() {
        return (
            <StarbucksMap 
            configs={configs.configs} 
            action={this.changeBounds}
            bounds={this.state.bounds}
            />          
        );
    }
  }