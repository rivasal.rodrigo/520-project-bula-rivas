import React, { Component } from "react";
import { 
  MapContainer, 
  TileLayer, 
  CircleMarker,
  Popup, 
} from "react-leaflet";
import MarkerClusterGroup from "react-leaflet-markercluster";
import "leaflet/dist/leaflet.css";
import "react-leaflet-markercluster/dist/styles.min.css";
import StarbucksToolTip from "./StarbucksToolTip";
import Bounds from "./Bounds";

export default class StarbucksMap extends Component {
  constructor(props){
    super(props);
    this.state = {
        Stores: [],
        selectedStore: null,
        coordinates: [],
        isFetching: false,
    }
  }

  async componentDidMount() {
    await this.fetchStores();
  }

  async componentDidUpdate(prevProps, prevState){
    if(prevProps.bounds!== this.props.bounds){
      await this.fetchStores();
    }
  }

  async fetchStores(){
    const urlFetch = `/api/stores/?nElon=${this.props.bounds[0][0]}&nElat=${this.props.bounds[0][1]}&sWlon=${this.props.bounds[1][0]}&sWlat=${this.props.bounds[1][1]}`;
    const stores = await (await fetch(urlFetch)).json();
    this.setState({Stores: stores, isFetching: false});
  }

  render() {
    return (
        <MapContainer
          center={this.props.configs.center}
          zoom={this.props.configs.zoom.initialZoom}
          zoomControl={false}
          style={{ width: "80vw",height: "80vh", position: "absolute", top: "12vh", left: "12vw", zIndex: -1, }}
          updateWhenZooming={false}
          updateWhenIdle={true}
          preferCanvas={true}
          minZoom={this.props.configs.zoom.minZoom}
          maxZoom={this.props.configs.zoom.maxZoom}
        >
          <TileLayer
            attribution={this.props.configs.attribution}
            url={this.props.configs.tileUrl}
          />
          
          <MarkerClusterGroup
            spiderfyOnMaxZoom={true}
            zoomToBoundsOnClick={true}
            showCoverageOnHover={true}
            removeOutsideVisibleBounds={false}
            disableClusteringAtZoom={18}> 
            {this.state.Stores.map((item, index) => 
              <CircleMarker 
                key={index}
                color={"red"}
                opacity={1}
                radius={5}
                weight={1}
                center={[item.Geo.coordinates[1], item.Geo.coordinates[0]]}
                eventHandlers={{
                  click: () => {
                    this.setState({ selectedStore: item, coordinates: [item.Geo.coordinates[1], item.Geo.coordinates[0]]});                    
                  },
                }}/>
            )}  
          </MarkerClusterGroup>
                {this.state.selectedStore != null &&
                  <Popup position={this.state.coordinates} onClose={()=> this.setState({selectedStore: null})}>
                  <StarbucksToolTip store={this.state.selectedStore}/>
                  </Popup>
                }
              <Bounds action={this.props.action}/>

        </MapContainer>
    );
  }
}
