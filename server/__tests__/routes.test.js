const request = require("supertest")
const app = require("../app")
const DAO = require("../db/conn")
const{expect} = require("@jest/globals");

jest.mock("../db/conn")

test("GET /", async () => {
  jest.spyOn(DAO.prototype, "getAllDocuments").mockResolvedValue("All documents");
  const response = await request(app).get("/api/")
  expect(response.text).toEqual("All documents")
});

test("GET /store/:id", async () => {
  jest.spyOn(DAO.prototype, "getSingleDocument").mockResolvedValue("One document");
  const response = await request(app).get("/api/store/1234")
  expect(response.text).toEqual("One document")
});

test("GET /stores/", async () => {
  jest.spyOn(DAO.prototype, "getDocumentsGeo").mockResolvedValue("Polygon of documents");
  const response = await request(app).get("/api/stores/")
  expect(response.text).toEqual("Polygon of documents")
});