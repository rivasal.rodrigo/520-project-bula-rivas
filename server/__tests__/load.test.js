const load = require("../utils/load")
const{expect} = require("@jest/globals");

/**
 * This tests if the file exists and gets the first value 
 * of the csv.
 */
test("file found and parsed", async () => {
  const data = await load("./__tests__/testfile.csv");
  expect(data[0]["Store Name"]).toMatch("Meritxell, 96");
});
