const load = require("./load");
const DAO = require("../db/conn");
const db = new DAO();
const index = {"Geo": "2dsphere"}

async function writeToDb() {
  try {

    const data = await load("./server/utils/directory.csv")
    await db.connect("starbucks", "stores");
    await db.insertMany(data);
    await db.createIndex(index);
    await db.disconnect();
  } catch (error) {
    console.log("could not connect");
  }

}

module.exports = writeToDb;