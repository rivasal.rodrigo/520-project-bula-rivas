
const csv = require("csvtojson");

async function read(filename) {
  try {
    const data = await csv().fromFile(filename)
    const cleanData = await clean(data);
    return cleanData;
  } catch (err) {
    console.error(err.message);
    throw err;
  }
}


async function clean(data){
  data.forEach((element, index, object) => {
    let longitude = element["Longitude"];
    let latitude  = element["Latitude"];

    if(longitude === "" || latitude === ""){
      object.splice(index, 1);
      return;
    }

    element.Geo = {"type" : "Point", "coordinates" : [parseFloat(longitude), parseFloat(latitude)]}
    delete element["Longitude"];
    delete element["Latitude"];
    delete element["Timezone"];
    delete element["Ownership Type"];
    delete element["Brand"];
    delete element["Store Number"];
  });

  return data;
}

module.exports = read;
