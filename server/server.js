const app = require("./app");
const DAO = require("./db/conn");

const PORT = process.env.PORT || 3001;

/** 
 * Sets the port to 3001 or the 
*/
app.set("port", PORT);

(async () => {
  try{
    /**
     * Connect to the database before starting to listen for connections
     */
    const db = new DAO();
    await db.connect("starbucks", "stores");
  }catch(e){
    console.error("could not connect");
    process.exit();
  }

  /** 
    * starts the server and listes on the port 3001 as defined
    * at the beginning of the js.
  */
  app.listen(app.get("port"), ()=>{
    console.log(`Server running on port ${app.get("port")}` )
  });

})();

