require("dotenv").config();
const dbUrl = process.env.ATLAS_URI;
const { MongoClient, ObjectId } = require("mongodb");

let instance = null;

class DAO {
  constructor(){
    if (!instance){
      instance = this;
      this.client = new MongoClient(dbUrl);
      this.db = null;
      this.collection = null;
    }
    return instance;
  }

  async connect(dbname, collName) {
    if (this.db){
      return;
    }
    await this.client.connect();
    this.db = await this.client.db(dbname);
    console.log("Successfully connected to MongoDB database " + dbname);
    this.collection = await this.db.collection(collName)
  }

  async insertMany(array) {
    let result = await this.collection.insertMany(array);
    return result.insertedCount;
  }

  async getAllDocuments(){
    let result = await this.collection.find({})
    return result.toArray();
  }

  async getSingleDocument(id){
    let result = await this.collection.findOne({"_id" : ObjectId(id)});
    return result;
  }

  async getDocumentsGeo(nElon, nElat, sWlon, sWlat){
    let northEast = [nElon, nElat];
    let southWest = [sWlon, sWlat];
    let result =  await this.collection.find({
      Geo: {
        $geoWithin:{
          $box:[
            southWest, northEast
          ]
        }
      }
    });
    return result.toArray();
  }

  async disconnect(){
    this.client.close();
  }

  async createIndex(obj){
    return await this.collection.createIndex(obj);
  }
}

module.exports = DAO;
