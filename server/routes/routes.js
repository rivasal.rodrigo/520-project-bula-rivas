const express = require("express");
const cache = require("memory-cache");
let router = express.Router();
const DAO = require("../db/conn")
const db = new DAO();

/**
 * @swagger
 * /api/:
 *  get:
 *    summary: Retrieve all the stores available in the database
 *    description: Retrieve all starbucks stores from the database. It can be used to populate a map with all the stores.
 *    responses:
 *      200:
 *         description: a list of stores.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 _id:
 *                   type: string
 *                   example: 618d55d56aaf6966b84ee09d
 *                 Store Name:
 *                   type: string
 *                   example: Yas Mall 3
 *                 Street Address:
 *                   type: string
 *                   example: YAS Island
 *                 City:
 *                   type: string
 *                   example: Abu Dhabi
 *                 State/Province:
 *                   type: string
 *                   example: AZ
 *                 Country:
 *                   type: string
 *                   example: AE
 *                 Postcode:
 *                   type: string
 *                   example: ""
 *                 Phone Number:
 *                   type: string
 *                   example: ""
 *                 Geo:
 *                   type: object
 *                   properties:
 *                     type:
 *                       type: string
 *                       example: Point
 *                     coordinates:
 *                       type: double
 *                       example: [54.6, 24.48]
 */
router.get("/", async function (req, res){
  console.log("All documents");
  try{
    let response = cache.get("getAll");
    if(!response){
      response = await db.getAllDocuments();
      cache.put("getAll", response)
    }
    res.send(response);
  } catch(e){
    res.send(e);
  }
});

/**
 * @swagger
 * /api/store/{id}:
 *  get:
 *    summary: Retrieve a store by its unique ID
 *    description: Retrieve all starbucks stores from the database. It can be used to populate a map with all the stores.
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: ID of the store to retrieve.
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *         description: A store located by its unique ID.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 _id:
 *                   type: string
 *                   example: 618d55d56aaf6966b84ee09d
 *                 Store Name:
 *                   type: string
 *                   example: Yas Mall 3
 *                 Street Address:
 *                   type: string
 *                   example: YAS Island
 *                 City:
 *                   type: string
 *                   example: Abu Dhabi
 *                 State/Province:
 *                   type: string
 *                   example: AZ
 *                 Country:
 *                   type: string
 *                   example: AE
 *                 Postcode:
 *                   type: string
 *                   example: ""
 *                 Phone Number:
 *                   type: string
 *                   example: ""
 *                 Geo:
 *                   type: object
 *                   properties:
 *                     type:
 *                       type: string
 *                       example: Point
 *                     coordinates:
 *                       type: double
 *                       example: [54.6, 24.48]
 */
router.get("/store/:id", async function(req, res){
  try{
    let storeId = req.params.id;
    let response = cache.get(storeId);
    if(!response){
      response = await db.getSingleDocument(storeId);
      cache.put(storeId, response)
    }
    res.send(response);
  }catch(e){
    res.send(e);
  }
});


/**
 * @swagger
 * /api/stores/:
 *  get:
 *    summary: Retrieve all the stores by a specified geospatial area
 *    description: Retrieves all starbucks stores from the database. It can be used to populate a map with all the stores.
 *    parameters:
 *      - in: query
 *        name: nElon
 *        required: true
 *        description: North East longitued.
 *        schema:
 *          type: number
 *      - in: query
 *        name: nElat
 *        required: true
 *        description: North East latitude.
 *        schema:
 *          type: number
 *      - in: query
 *        name: sWlon
 *        required: true
 *        description: South West longitued.
 *        schema:
 *          type: number
 *      - in: query
 *        name: sWlat
 *        required: true
 *        description: South West latitude.
 *        schema:
 *          type: number
 *    responses:
 *      200:
 *         description: A list of stores located by a specified geospatial area.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 _id:
 *                   type: string
 *                   example: 618d55d56aaf6966b84ee09d
 *                 Store Name:
 *                   type: string
 *                   example: Yas Mall 3
 *                 Street Address:
 *                   type: string
 *                   example: YAS Island
 *                 City:
 *                   type: string
 *                   example: Abu Dhabi
 *                 State/Province:
 *                   type: string
 *                   example: AZ
 *                 Country:
 *                   type: string
 *                   example: AE
 *                 Postcode:
 *                   type: string
 *                   example: ""
 *                 Phone Number:
 *                   type: string
 *                   example: ""
 *                 Geo:
 *                   type: object
 *                   properties:
 *                     type:
 *                       type: string
 *                       example: Point
 *                     coordinates:
 *                       type: double
 *                       example: [54.6, 24.48]
 */
router.get("/stores/", async function(req, res){
  let nElon = parseFloat(req.query.nElon);
  let nElat = parseFloat(req.query.nElat);
  let sWlon = parseFloat(req.query.sWlon);
  let sWlat = parseFloat(req.query.sWlat);

  try{
    let response = cache.get(nElon + nElat + sWlon + sWlat);
    if(!response){
      response = await db.getDocumentsGeo(nElon, nElat, sWlon, sWlat);
      cache.put(nElon + nElat + sWlon + sWlat, response)
    }

    res.send(response);
  }catch(e){
    res.send(e);
  }
})


module.exports = router;
