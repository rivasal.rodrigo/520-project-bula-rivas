const express = require("express");
const app = express();
const routes = require("./routes/routes");
const path = require("path");


//Compression import and implementation
const compression = require("compression");
app.use(compression());

//Swagger requirements
const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");


//Middleware for browser caching
app.use(function (req, res, next) {
  res.set("Cache-control", "public", "max-age=31536000");
  next();
});

//Routes
app.use("/api", routes);

//Swagger documentation settings
const swaggerDefinition = {
  openapi: "3.0.0",
  info: {
    title: "Express API for Starbucks Stores",
    description: "This is a REST API application made with Express. It retrieves data of starbucks stores from Kaggle"
  },
};

//Location to read the swagger tag
const options = {
  swaggerDefinition,
  apis:["./server/routes/*.js"],
}
  
const swaggerSpec = swaggerJSDoc(options);
  
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

//Middleware
app.use(express.static(path.join(__dirname, "../client/build")));

module.exports = app;