# 520-Project Starbucks Stores
## Packages used
    csvtojson by keyang
        https://www.npmjs.com/package//csvtojson
    
    This package was used to convert the data set to a json format.

## Dataset
The starbucks' dataset was obtained from [kaggle](https://www.kaggle.com/starbucks/store-locations) and it was scraped by [chrismeller](https://github.com/chrismeller/).


## Performance tests
The tests for our website will be run on Google chrome Version 96.0.4664.93(64-bit) using Incognito mode.

### **Lighthouse Tests**
When running a Lighthouse test on the website we get a score of 68 for Performance, 96 for accesibility, 93 for best practices adn 100 for SEO.
We get 760ms of total blocking. According to the report, the main causes for the poor performance are the fact that the images are not in next-gen formats, which could save us 0.45s. The 2nd big issue is not giving the icon on of the header an explicit width and height.
***
<br />

### **Performance test**
When running the performance test on chrome without throttling the program runs on a smooth 60 frames per second except when the map is moved towards regions with high amounts of starbucks stores like Chicago or New york, the program then takes a lot of time fetching the stores and loading them. When
running the site in slow 3G, the difference in performance is instantly visible. The map can stay idle for roughly 4-5 seconds before loading the chunks. Most of the time the program is idling while loading fetching the stores and loading the map. Much time is wasted in function calls.
*** 
<br />

### **Network**
When loading the app with no throttling, the DOMContentLoaded sits at 128ms and the Load is of 151ms. When testing the network with low 3G, the website takes 6.39s for DOMContentLoaded and 26.82s for Load. The thing that takes the most to load is the map which takes around 20 seconds to load.

When we enable cache, the DOMContentLoaded is at 69ms and Load at 72ms. Most of the tiles of the map are cache and many of them take 0ms to load.

When enabling cache on slow 3G, the website takes 4.22 to DOMContentLoadede and 4.22s to Load. This is a considerable upgrade compared to the 20 seconds it would take to load without cache.
***
<br />

### **Disabling Compression**
When we disable compression, the network metrics are a bit higher. Without cache It takes 165ms for the DOM to load and 164ms for Load. With cache it takes 104ms for the DOM to load and 106ms for Load.

When doing the performance test we have found that without compression, the site has a lot of frame loss due to the fetching, even in areas where we had no loading at all witht he compression on. 

We have also found that then when doing the Lighthouse test on the heroku app, we get scores of around 70 for the performance section. While if we run the tests using my localhost we get scores around 40.


    Project developed by Christian Bula and Rodrigo Rivas